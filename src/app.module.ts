import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemparetureModule } from './temperature/tempareture.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [TemparetureModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
