/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TemperatureController } from './temperature.controller';
import { TemperatureService } from './tempareture.service';

@Module({
    imports: [],
    exports: [],
    controllers: [TemperatureController],
    providers: [TemperatureService],
})
export class TemparetureModule {}